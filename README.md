   
   
**Cost Forecast** application is to forecast the cost of healthcare IT infrastructure based on the customer usage and expected growth. It is an open source, an easy to deploy application via docker created in pure PHP or without using any framework and libraries.  

In addition, a technical exam for software engineer applicant of [LifeTrackMed](https://lifetrackmed.com/)


[![Dashboard](https://www.goheytech.com/assets/img/gallery/cost-forecast.JPG)](https://www.goheytech.com/assets/img/gallery/cost-forecast.JPG)


* [System requirements](#system-requirements)
* [Features](#features)
* [Installing](#installing)
* [Installing via Docker](#installing-via-docker)
* [Author](#author)


## System requirements

* Any operating system

* PHP 7.4+ 

* Web-server (Apache 2.4)

* Database (MySQL 8+)

* Docker


## Features

* Clone project from [Gitlab](https://gitlab.com/gemrald/cost-forecast);

* Can be deployed either manually or docker;

* Build from pure PHP, Javascript/AJAX and Html/CSS3;

* Parameters or constants for computation can be changeable;

* Database-driven application;

* Computes monthly forecasted cost (RAM and Storage);

* Docker repository is available at Docker Hub (https://hub.docker.com/repository/docker/gemrald/cost-forecast)


## Installing

* Download and copy the application from your web server's directory:

```bash
cd /var/www/html/cost-forecast (in linux)
```

* Create the database and/or import table via commandline:

```bash
Commands:
mysql -u root -p;
mysql> CREATE DATABASE forecast_db;
mysql> CREATE TABLE costs;
mysql> exit;

Or import the sql file (dump.sql) from /dump directory or folder:
mysql -u root -p forecast_db < dump.sql
```

Or download the file and manually create the database and table in your preferred DB client tool.

* After creating the DB, import the dump.sql as an alternative.

* Or better yet, run the script via accessing the url at: [http://localhost/core/migrate.php](http://localhost/core/migrate.php) and it will import or create the costs table automatically.

* Once done, access the application or visit the url at http://localhost or http://127.0.0.1


## Installing via Docker

If you want to install the application as a Docker container, please do the following.


* Clone this project on your preferred location and/or pull the latest code from the repository by Git (If you want the latest `master` branch):

    ```bash
    Clone:
    git clone git@gitlab.com:gemrald/cost-forecast.git

    Pull (if necessary):
    git checkout master
    git pull 
    ```
* Go to your Cost Forecast directory (to `/var/www/html/cost-forecast` for example):

    ```bash
    cd /var/www/html/cost-forecast
    ```

* From the /cost-forecast directory, execute the command:

    ```bash
    docker-compose up
    ```

* To stop the container:

    ```bash
    docker-compose down
    ```
* If you want to rebuild the container:

    ```bash
    docker-compose up --build
    ```
* To check and access the database, just login at http://localhost:8080 and enter the following details:

    ```bash
    Server: db 
    Username: root 
    Password: root 
    Database: forecast_db
    ```

* Once done, just visit the application via [http://localhost](http://localhost) or [http://127.0.0.1](http://127.0.0.1)


## Author

Hi, I'm Gemrald R. Calibara, please visit my online portfolio @ [https://www.goheytech.com](https://www.goheytech.com)
