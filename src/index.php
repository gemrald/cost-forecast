<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/style.css">

    <title>Cost Forecast of Infrastructure</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">

    <div class="container">
        <h2>Cost Forecast of Infrastructure</h2>
        <form id="formCostForecast">
            <div class="row">
                <div class="col-25">
                    <label for="numStudyPerDay">Number of study per day</label>
                </div>
                <div class="col-75">
                    <input type="number" id="numStudyPerDay" name="numStudyPerDay" value="1000">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="studyGrowthPerMonthPercent">Study growth per month in %</label>
                </div>
                <div class="col-75">
                    <input type="number" id="studyGrowthPerMonthPercent" name="studyGrowthPerMonthPercent" value="3">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="numMonthToForecast">Number of months to forecast</label>
                </div>
                <div class="col-75">
                    <input type="number" id="numMonthToForecast" name="numMonthToForecast" min="1" max="12" value="12">
                </div>
            </div>
            <div class="row">
                <input type="submit" value="Compute">
            </div>
        </form>
    </div>

    <table id="tabCostForecast">
        <tr>
            <th>Month Year</th>
            <th>Number of Studies</th>
            <th>Cost Forecasted</th>
        </tr>
        <tr>
            <td>N/A</td>
            <td>N/A</td>
            <td>N/A</td>
        </tr>
    </table>
    <script src="/js/main.js"></script>
</body>

</html>