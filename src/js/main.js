window.addEventListener("load", () => {

    const formData = document.getElementById("formCostForecast");

    formData.addEventListener("submit", async (e) => {
        e.preventDefault();

        const localhost = window.location.hostname;
        const apiUrl = `http://${localhost}/core/api.php`;
        const numStudyPerDay = document.getElementById('numStudyPerDay').value;
        const studyGrowthPerMonthPercent = document.getElementById('studyGrowthPerMonthPercent').value;
        const numMonthToForecast = document.getElementById('numMonthToForecast').value;

        fetch(apiUrl, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json', // sent request
                    'Accept': 'application/json' // expected data sent back
                },
                body: JSON.stringify({
                    numStudyPerDay: numStudyPerDay,
                    studyGrowthPerMonthPercent: studyGrowthPerMonthPercent,
                    numMonthToForecast: numMonthToForecast
                })
            })
            .then(response => response.json())
            .then(json => {

                let tab =
                    `<tr> 
            <th>Month Year</th> 
            <th>Number of Studies</th> 
            <th>Cost Forecasted</th> 
            </tr>`;

                json.forEach(row => {
                    var monthYear = row.month_year;
                    var studies = parseFloat(row.total_studies_per_month).toLocaleString('en');
                    var cost = parseFloat(row.total_cost_per_month).toLocaleString('en');
                    tab += `<tr>  
                <td>${monthYear}</td> 
                <td>${studies}</td> 
                <td>$${cost}</td>          
                </tr>`;
                });

                // Display result 
                document.getElementById("tabCostForecast").innerHTML = tab;

            }).catch((error) => console.log(error))

    });
});