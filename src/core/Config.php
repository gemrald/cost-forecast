<?php

class Config
{
    private const DB_HOST = 'db';
    private const DB_USER = 'root';
    private const DB_PASS = 'root';
    private const DB_NAME = 'forecast_db';

    private $dsn =
        'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . '';

    protected $conn = null;

    // Method for connection to the database
    public function __construct()
    {
        try {
            $this->conn = new PDO($this->dsn, self::DB_USER, self::DB_PASS);
            $this->conn->setAttribute(
                PDO::ATTR_DEFAULT_FETCH_MODE,
                PDO::FETCH_ASSOC
            );
        } catch (PDOException $e) {
            die('Error: ' . $e->getMessage());
        }
    }
}
