<?php

$host = "db";
$db = "forecast_db";

/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with password root) */
try{
    
    $pdo = new PDO("mysql:host=$host;dbname=$db", "root", "root");
    
    // Set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Attempt create table query execution
    $sql = "CREATE TABLE costs(
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        month_year varchar(8) DEFAULT '0',
        total_studies_per_month int(11) unsigned DEFAULT '0',
        total_cost_per_month decimal(10,2) DEFAULT '0.00',
        PRIMARY KEY (`id`)
    )";    
    $pdo->exec($sql);

    echo "Costs table created successfully.";
} catch(PDOException $e){
    die("ERROR: Could not able to execute $sql. " . $e->getMessage());
}
 
// Close connection
unset($pdo);
