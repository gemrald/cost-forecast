<?php

require_once '../core/Config.php';

class Database extends Config
{
    /**
     * Inserts the data
     *
     * @param  mixed $monthYear
     * @param  mixed $totalStudies
     * @param  mixed $totalCost
     * @return true
     */
    public function insert($monthYear, $totalStudies, $totalCost)
    {
        $sql =
            'INSERT INTO costs (month_year, total_studies_per_month, total_cost_per_month) VALUES (:month_year, :total_studies_per_month, :total_cost_per_month)';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([
            'month_year' => $monthYear,
            'total_studies_per_month' => $totalStudies,
            'total_cost_per_month' => $totalCost,
        ]);

        return true;
    }

    /**
     * Gets the data
     *
     * @return array
     */
    public function fetchAllData()
    {
        $sql =
            'SELECT month_year, total_studies_per_month, total_cost_per_month FROM costs ORDER BY id ASC';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Clears the data
     *
     * @return true
     */
    public function deleteAllData()
    {
        $sql = 'DELETE FROM costs';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return true;
    }
}
