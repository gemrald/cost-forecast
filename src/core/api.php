<?php

require_once '../core/CostForecast.php';

$contentType = isset($_SERVER["CONTENT_TYPE"])
    ? trim($_SERVER["CONTENT_TYPE"])
    : '';

if ($contentType === "application/json") {
    //Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));

    $postData = json_decode($content, true);

    //If json_decode failed, the JSON is invalid.
    if (
        is_array($postData) &&
        isset($postData['numStudyPerDay']) &&
        isset($postData['studyGrowthPerMonthPercent']) &&
        isset($postData['numMonthToForecast'])
    ) {
        $cost = new CostForecast();
        $response = $cost->getComputedCostForecast(
            $postData['numStudyPerDay'],
            $postData['studyGrowthPerMonthPercent'],
            $postData['numMonthToForecast']
        );

        echo json_encode($response);
    } else {
        echo json_encode(['error' => 'All input fields must be filled.']);
    }
}
