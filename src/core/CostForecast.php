<?php

require_once '../core/Database.php';

class CostForecast
{
    public $year;
    public $numberStudiesPerFiveHundredMbOfRam;
    public $numberMbOfRamPerThousandStudies;
    public $costHourlyGbOfRam;
    public $numberMbOfStoragePerStudy;
    public $costMonthlyGbOfStorage;
    public $numberHoursPerDay;
    public $oneGbToMbRatio;
    public $db;

    /**
     * Initialize variables
     */
    public function __construct()
    {
        $this->year = date('Y');
        $this->numberStudiesPerFiveHundredMbOfRam = 1000;
        $this->numberMbOfRamPerThousandStudies = 500;
        $this->costHourlyGbOfRam = 0.00553;
        $this->numberMbOfStoragePerStudy = 10;
        $this->costMonthlyGbOfStorage = 0.1;
        $this->numberHoursPerDay = 24;
        $this->oneGbToMbRatio = 1000;

        $this->db = new Database();

        //Clear all data first
        $this->db->deleteAllData();
    }

    /**
     * Compute cost forecasted
     *
     * @param  mixed $numStudyPerDay
     * @param  mixed $studyGrowthPerMonthPercent
     * @param  mixed $numMonthToForecast
     * @return array
     */
    public function getComputedCostForecast(
        $numStudyPerDay,
        $studyGrowthPerMonthPercent,
        $numMonthToForecast
    ) {
        try {
            for ($month = 1; $month < $numMonthToForecast + 1; $month++) {
                //Count number of days for each month
                $countDaysOfMonth = cal_days_in_month(
                    CAL_GREGORIAN,
                    $month,
                    $this->year
                );

                //Display month and year
                $monthYear =
                    date("M", mktime(0, 0, 0, $month, 1, $this->year)) .
                    ' ' .
                    $this->year;

                //Compute for new number of studies per month
                $newNumberStudyPerMonth = $numStudyPerDay * $countDaysOfMonth;

                //Compute for Total Number of Studies per month with Growth Percentage
                $totalNumberStudyPerMonthWithGrowthPercentage =
                    $newNumberStudyPerMonth +
                    $newNumberStudyPerMonth *
                        ($studyGrowthPerMonthPercent / 100);

                //Compute cost of RAM per month
                $costGbOfRamPerMonth = $this->computeCostOfRam(
                    $totalNumberStudyPerMonthWithGrowthPercentage
                );

                //Compute cost of storage per month
                $costGbOfStoragePerMonth = $this->computeCostOfStorage(
                    $totalNumberStudyPerMonthWithGrowthPercentage
                );

                //Total cost forecasted per month
                $totalCostForecastedPerMonth = round(
                    $costGbOfRamPerMonth + $costGbOfStoragePerMonth,
                    2
                );

                //Save data to DB
                $this->db->insert(
                    $monthYear,
                    $totalNumberStudyPerMonthWithGrowthPercentage,
                    $totalCostForecastedPerMonth
                );
            }

            //Display all data
            $response = $this->db->fetchAllData();

            return $response;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    /**
     * Compute monthly cost of RAM
     *
     * @param  mixed $totalStudies
     * @return float
     */
    public function computeCostOfRam($totalStudies)
    {
        $countNumberMbOfRamToStudiesRatio =
            $totalStudies / $this->numberStudiesPerFiveHundredMbOfRam;
        $totalMbOfRam =
            $countNumberMbOfRamToStudiesRatio *
            $this->numberMbOfRamPerThousandStudies;
        $convertMbToGbOfRam = $totalMbOfRam / $this->oneGbToMbRatio;
        $costGbOfRamPerMonth =
            $convertMbToGbOfRam *
            $this->costHourlyGbOfRam *
            $this->numberHoursPerDay;

        return floatval($costGbOfRamPerMonth);
    }

    /**
     * Compute monthly cost of storage
     *
     * @param  mixed $totalStudies
     * @return float
     */
    public function computeCostOfStorage($totalStudies)
    {
        $convertTenMbToGbOfStorage =
            $this->numberMbOfStoragePerStudy / $this->oneGbToMbRatio;
        $countNumberMbOfRamToStudiesRatio =
            $totalStudies * $convertTenMbToGbOfStorage;
        $costGbOfStoragePerMonth =
            $countNumberMbOfRamToStudiesRatio * $this->costMonthlyGbOfStorage;

        return floatval($costGbOfStoragePerMonth);
    }
}
